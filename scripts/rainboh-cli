#!/usr/bin/env python3
"""
    Copyright 2020 inbitcoin s.r.l.

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""

"""
curl -H "Accept: application/json" \
     -H "Content-type: application/json" \
     -X POST \
     -d '{"jsonrpc": "1.0", "id":"curltest", "method": "<method name>", "params": [] }' <url>
"""

import sys
import json

from os import environ as env
from requests import post
from requests.exceptions import RequestException


def _print_help():
    print('Usage: {} [options] <method> [[[param0] param1] ...]'.format(sys.argv[0]))
    print('''
Options:
    -rpcconnect=<host>          set rainboh-x host
    -rpcport=<port>             set rainboh-x port
    -rpcwallet=<wallet>         set wallet to use''')
    print('''
Methods:
    == Control ==
    help

    == Blockchain ==
    getblock "blockhash" ( verbosity )
    getblockcount
    getblockhash height

    == Network ==
    getconnectioncount
    getpeerinfo

    == Rawtransactions ==
    decoderawtransaction "hexstring" ( iswitness )
    finalizepsbt "psbt"
    getrawtransaction "txid" ( verbose "blockhash" )
    sendrawtransaction "hexstring"

    == Wallet ==
    createpsbt [{"txid":"hex","vout":n},...] [{"address":amount},{"address":{"amount":amount,"amountBtc":amount}},...]
    createwallet "wallet_name"
    encryptwallet "passphrase"
    getbalance ( "*" minconf )
    getnewaddress
    gettransaction "txid"
    listtransactions ( "*" count skip )
    listunspent ( minconf maxconf )
    listwallets
    loadwallet "filename"
    sendtoaddress "address" amount ( dummy dummy dummy dummy conf_target )
    sendmany "" amounts
    unloadwallet ( "wallet_name" )
    walletlock
    walletpassphrase "passphrase" timeout
    walletpassphrasechange "oldpassphrase" "newpassphrase"
    walletprocesspsbt "psbt"

    == Utility ==
    estimatesmartfee conf_target
    getconfig
''')


PORT = env.get('PORT', 7872)
HOST = env.get('HOST', '127.0.0.1')
METHODS = '''createwallet loadwallet unloadwallet listwallets getnewaddress getbalance listunspent issueasset sendmany
getblockhash getblockcount getblock sendtoaddress walletpassphrase walletlock walletpassphrasechange encryptwallet
getpeerinfo listtransactions getconnectioncount gettransaction reloadconfig getconfig loadrawconfig opreturnlimit'''.split()

METHODS.append('getrawtransaction')
METHODS.append('decoderawtransaction')
METHODS.append('sendrawtransaction')
METHODS.append('walletprocesspsbt')
METHODS.append('finalizepsbt')
METHODS.append('estimatesmartfee')
METHODS.append('createpsbt')

METHODS.append('kaspa_createwallet')
METHODS.append('kaspa_getnewaddress')
METHODS.append('kaspa_listaccounts')
METHODS.append('kaspa_spend')
METHODS.append('kaspa_isvalidaddress')

HEADERS = {
    'Accept': 'application/json',
    'Content-type': 'application/json'
}

WALLET = ''

for arg in sys.argv[:]:
    if '-rpcconnect=' in arg:
        HOST = arg.split('=')[1]
        sys.argv.remove(arg)
    if '-rpcport=' in arg:
        PORT = arg.split('=')[1]
        sys.argv.remove(arg)
    if '-rpcwallet=' in arg:
        WALLET = arg.split('=')[1]
        sys.argv.remove(arg)

URL = 'http://{}:{}'.format(HOST, PORT)

if WALLET:
    URL = 'http://{}:{}/wallet/{}'.format(HOST, PORT, WALLET)

if len(sys.argv) < 2:
    _print_help()
    sys.exit(1)


method = sys.argv[1]
params = sys.argv[2:]


if method == 'help':
    _print_help()
    sys.exit()


if method not in METHODS:
    print('Unknown method')
    sys.exit(2)


def detect_types(params):
    for i, p in enumerate(params):
        try:
            params[i] = json.loads(p)
        except json.JSONDecodeError:
            pass

    return params


data = {
    'jsonrpc': '1.0',
    'id': 'curltest',
    'method': method,
    'params': detect_types(params),
}

try:
    r = post(url=URL, data=json.dumps(data), headers=HEADERS)
    print(json.dumps(r.json(), indent=2))
except RequestException as err:
    print('ERR:', err)

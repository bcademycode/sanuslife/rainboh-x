# Rainboh-x

Rainboh-x is a wrapper of Bitcoin Core that provides multi-colored-coins functionality.

- [Concept](#concept)
- [Features](#features)
- [Requirements](#requirements)
- [Configuring](#configuring)
- [Installing and running](#installing-and-running)
- [How to use](#how-to-use)
- [Example](#example)
- [API documentation](#api-documentation)
- [Licensing](#licensing)
- [Contributing](#contributing)

## Concept

Rainboh-x allows you to create and manage pairs of a colored and a bitcoin wallet:

- **colored wallet**: stores, receives and sends colored coins
- **bitcoin wallet**: stores, receives and sends bitcoins; its main purpose is to fund
  colored transaction fees

Both must be defined in the configuration file (see [Configuration](#configuration)).
The colored wallet must be linked to its bitcoin wallet counterpart via the `btcWallet` parameter.

### Colored wallet: characteristics

- addresses are always preceded by the prefix `addressPrefix`
- `balance` and `amount` are in token currency
- for each ouput parameter `balance` there is a `balanceBtc`
- for each ouput parameter `amount` there is an `amountBtc`

**Caution:**

- before using **colored wallet** `sendtoaddress` and `sendmany` both wallets must be loaded and, if encrypted,
  unlocked
- only legacy addresses can be generated
- avoid using bitcoind directly on these wallets

## Features

- [JSON-RPC 1.0 Specification](https://www.jsonrpc.org/specification_v1) compliant,
  we strive to keep an interface very similar to the one of
  [bitcoind v0.18.x](https://bitcoincore.org/en/doc/0.18.0/)
- supports multi-coin and multi-wallet per coin
- helps preventing accidental burning of coins

## Requirements

- Linux <sup>1</sup>
- Node.js LTS 12
- yarn
- git
- [Bitcoin Core](https://github.com/bitcoin/bitcoin) 0.18.x <sup>2</sup>
- [Colored Coins Block-Explorer](https://github.com/inbitcoin/cexplorer)

1. _tested on Debian 10 Buster_
2. _tested with 0.18.1_

## Configuring

On a first run, copy [`rainboh-config.sample.json`](rainboh-config.sample.json) as
`rainboh-config.json`.
Then, to edit the configuration parameters follow [CONFIGURATION.md](docs/CONFIGURATION.md).

## Installing and running

To install and run, execute:

```bash
yarn install
yarn build-ts
yarn start
```
## Build and running on docker
### Development environment
A Dockerfile based on node 12 Debian Bullseye is available.
To build and run a docker image of rainboh-x, run:

```bash
./dev build
./dev up
```

### Production environment with multiarch support and push to repository
```bash
MULTI_ARCH=yes ./dev build-prod
MULTI_ARCH=yes ./dev push-prod
```

## How to use

Rainboh-x exposes an JSON-RPC interface, available by default on port `7872`.

```bash
curl -H "Accept: application/json" \
     -H "Content-type: application/json" \
     -X POST \
     -d '{"jsonrpc": "1.0", "id":"curltest", "method": "<method_name>", "params": [] }' 127.0.0.1:7872/wallet/<wallet_name>
```

A CLI to send RPC commands to rainboh-x is available as `rainboh-cli` in the `scripts` directory.

```bash
./scripts/rainboh-cli [-rpcwallet=<wallet_name>] <method_name>
```

## Run tests

```bash
LOG_LEVEL=error yarn test
```

It is possible to run only a part of the tests. For example:

```bash
LOG_LEVEL=error yarn test -t='WalletCol.sendmany'
```

## Example

Create a first token wallet named "mywallet_tkn"

```bash
./scripts/rainboh-cli createwallet mywallet_tkn
{
  "error": null,
  "id": "curltest",
  "result": {
    "warning": "",
    "name": "mywallet_tkn"
  }
}
```

Check wallet balance

```bash
./scripts/rainboh-cli -rpcwallet=mywallet_tkn getbalance
{
  "error": null,
  "id": "curltest",
  "result": 0
}
```

Get a new address

```bash
./scripts/rainboh-cli -rpcwallet=mywallet_tkn getnewaddress "" legacy
{
  "result": "SmjBrVLdjYuK4DfZAxravtpUXKyk12SAsrL",
  "error": null,
  "id": "curltest"
}

```

After funds have been sent, check the balance (including unconfirmed UTXOs)

```bash
./scripts/rainboh-cli -rpcwallet=mywallet_tkn getbalance
{
  "error": null,
  "id": "curltest",
  "result": 42
}
```

To receive a reliable balance, you can get a balance that considers a specified number of
confirmations (3, as in this example, is the recommended minimum)

```bash
./scripts/rainboh-cli -rpcwallet=mywallet_tkn getbalance '*' 3
{
  "error": null,
  "id": "curltest",
  "result": 40
}
```

## API documentation

See [SPEC.md](docs/SPEC.md#supported-apis) for a list of available APIs.

## Licensing

Rainboh-x is licensed under the Apache License, Version 2.0.
See [LICENSE](LICENSE) for the full license text.

## Contributing

All contributions to rainboh-x are welcome.
To learn more on how to contribute, continue reading [here](CONTRIBUTING.md).

# Changelog

All notable changes to this project will be documented in this file.

The format is based on
[Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to
[Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.4.3] - 2022-06-22

### Added
- support to bitcoind 23

## [0.4.2] - 2021-12-06

### Added
- support to bitcoind 0.21 and 22 versions

## [0.4.0] - 2020-08-25

### Added
- Segwit support
- address_type parameter of getnewaddress

### Changed
- Bump config version from 1.1 to 2.0
- Use bech32 change addresses
- Wallets are not compatible with the previous versions after the upgrade

## [0.3.2] - 2020-06-24

### Fixed
- The parameter minconf of getbalance

### Changed
- Docker image optimizations

## [0.3.1] - 2020-06-11

### Changed
- Bump config version from 1.0 to 1.1

### Added
- Config bitcoind version validation
- Config version type validation

## [0.3.0] - 2020-05-26

### Added
- Bitcoind rpc timeout
- Rainboh-x rpc timeout
- Add conf_target parameter in colored sendtoaddress

### Changed
- Fix input selection createpsb
- Does not return sensitive information in getconfig

## [0.2.0] - 2020-05-05

### Added
- Test suite
- Gitlab CI
- Implemented sendmany API
- Sendmany supports colored and bitcoin amounts
- Implemented issueasset API for test env
- Check collection on DB connection
- Implemented opreturnlimit API
- Implemented getrawtransaction API
- Implemented colored decoderawtransaction API
- Implemented sendrawtransaction API
- Implemented walletprocesspsbt API
- Implemented finalizepsbt API
- Implemented estimatesmartfee API
- Implemented createpsbt API
- Createpsbt supports colored and bitcoin amounts

## [0.1.0] - 2020-02-13

### Added
- JSON-RPC 1.0 API interface
- JSON configuration with runtime validation
- Support for multi-coin an multi-wallet per coin
- Logging
- Documentation
- Dockerfile

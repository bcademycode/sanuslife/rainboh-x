FROM node:20-bullseye-slim as builder

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        ca-certificates git

ENV APP_DIR="/srv/app"

COPY src "$APP_DIR/src"
COPY kaspa "$APP_DIR/kaspa"
COPY package.json yarn.lock tsconfig.json "$APP_DIR/"

WORKDIR "$APP_DIR"

RUN yarn install \
  && yarn build-ts

FROM node:20-bullseye-slim

ENV APP_DIR="/srv/app" USER="node" NODE_ENV="production"
ENV LANG="C.UTF-8" PYTHONUNBUFFERED=1 PYTHONIOENCODING="UTF-8"

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        gosu python3-pip \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

RUN pip3 install requests~=2.22.0

COPY --from=builder --chown=$USER:$USER "$APP_DIR/dist" "$APP_DIR/dist"
COPY --from=builder --chown=$USER:$USER "$APP_DIR/node_modules" "$APP_DIR/node_modules"
COPY --chown=$USER:$USER package.json "$APP_DIR/package.json"
COPY --chown=$USER:$USER scripts/rainboh-cli "$APP_DIR/scripts/rainboh-cli"

COPY entrypoint.sh /usr/local/bin/entrypoint.sh
COPY kaspa "$APP_DIR/kaspa"
RUN chmod +x /usr/local/bin/entrypoint.sh

WORKDIR "$APP_DIR"
EXPOSE 7872
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]

/*
 *    Copyright 2024 Bcademy s.r.l.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import WalletKaspa from '../lib/kaspa/wallet'
import RpcHelper from '../utils/rpcHelper'
import RainbohError from '../error/rainbohError'

/**
 * Create kaspa address inside default wallet
 */
export async function kaspa_getnewaddress(args: any, context: any, done: (err: any, res?: any) => void) {
  try {
    const wallet = await WalletKaspa.create()
    done(null, await wallet.getNewAddress(args[0]))
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 * List accounts inside default wallet
 */
export async function kaspa_listaccounts(args: any, context: any, done: (err: any, res?: any) => void) {
  try {
    const count = args[0] || 100
    const skip = args[1] || 0
    const wallet = await WalletKaspa.create()
    const accounts = await wallet.listAccounts(count, skip)
    done(null, accounts)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 * Spend entire user balance to our wallet
 */
export async function kaspa_spend(args: any, context: any, done: (err: any, res?: any) => void) {
  try {
    if (args[0] === undefined || args[1] === undefined)
      return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())
    const accountIndex = args[0]
    const recipient = args[1]
    const wallet = await WalletKaspa.create()
    const transactionsIds = await wallet.spend(accountIndex, recipient)
    done(null, transactionsIds)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 * Validate kaspa address
 */
export async function kaspa_isvalidaddress(args: any, context: any, done: (err: any, res?: any) => void) {
  try {
    if (args[0] === undefined) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())
    const address = args[0]
    const wallet = await WalletKaspa.create()
    const addressCheck = wallet.isValidAddress(address)
    done(null, addressCheck)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/*
 *    Copyright 2020 inbitcoin s.r.l.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import RainbohError from '../error/rainbohError'
import ConfigProvider from '../utils/configProvider'
import RpcHelper from '../utils/rpcHelper'
import WalletBtc from '../lib/walletBtc'
import WalletCol from '../lib/walletCol'

/**
 * Load full or partial json configuration provided as parameter
 */
export function loadrawconfig(args: any, context: any, done: (err: any, res?: any) => void) {
  const configStr = args[0]

  if (!configStr || args[1]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())

  try {
    const configJson = JSON.parse(configStr)
    ConfigProvider.getInstance().loadConfigData(configJson)

    done(null, true)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

/**
 * Create a new asset in an address without prefix.
 * Returns the transaction ID <txid> and <assetId> if successful.
 */
export async function issueasset(args: any, context: any, done: (err: any, res?: any) => void) {
  const address = args[0]
  const amount = args[1]
  const divisibility = args[2]

  if (!address || !amount || args[3]) return done(new RainbohError(RainbohError.RPC_INVALID_PARAMS).toObject())
  if (!context.walletName || context.walletType === ConfigProvider.WALLET_TYPE_UNKNOWN)
    return done(new RainbohError(RainbohError.WALLET_NOT_SPECIFIED).toObject())
  if (context.walletType !== ConfigProvider.WALLET_TYPE_BITCOIN)
    return done(new RainbohError(RainbohError.WALLET_BTC_ONLY).toObject())

  const wallet = new WalletBtc(context.walletName)

  try {
    const txid = await wallet.issueAsset(address, amount, divisibility)
    return done(null, txid)
  } catch (err) {
    RpcHelper.logError(err)
    return done(RpcHelper.getErrorObj(err))
  }
}

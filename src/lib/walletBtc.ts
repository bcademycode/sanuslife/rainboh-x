/*
 *    Copyright 2020 inbitcoin s.r.l.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import Wallet from './wallet'
import ColoredHelper from '../utils/coloredHelper'
import { IUtxo as CapiUtxo } from '../interfaces/capi'
import { ICreatePsbtInputs, IBtcCreatePsbtOutputs, IBtcSendManyOutputs } from '../interfaces/wallet'
import { IWalletBtcConfig } from '../interfaces/rainbohConfig'
import { EstimateMode } from '../interfaces/wallet'
import RainbohError from '../error/rainbohError'
import BitcoinHelper from '../utils/bitcoinHelper'
import logger from '../utils/logger'

class WalletBtc extends Wallet {
  private readonly walletConfig: IWalletBtcConfig

  constructor(walletName: string) {
    super(walletName)

    this.walletConfig = this.configProvider.getWalletBtcConfig(walletName)
  }

  public getNewAddress(addressType?: 'legacy' | 'p2sh-segwit' | 'bech32'): Promise<string> {
    const label = ''

    if (!addressType) addressType = 'bech32'

    if (addressType !== 'legacy' && addressType !== 'p2sh-segwit' && addressType !== 'bech32')
      throw new RainbohError(RainbohError.INVALID_ADDRESS_TYPE)

    return this.coreClient.getNewAddress(label, addressType)
  }

  public getRawChangeAddress(): Promise<string> {
    const addressType = 'bech32'

    return this.coreClient.getRawChangeAddress(addressType)
  }

  public getBalance(minConf: number): Promise<any> {
    return this.coreClient.getBalance('*', minConf)
  }

  public listUnspent(minConf: number = 0, maxConf: number = 9999999): Promise<any> {
    return this.coreClient.listUnspent(minConf, maxConf)
  }

  public listTransactions(label: string, count: number, skip: number): Promise<any> {
    return this.coreClient.listTransactions(label, count, skip)
  }

  public getTransaction(txid: string): Promise<any> {
    return this.coreClient.getTransaction(txid)
  }

  public sendToAddress(address: string, amount: number, confTarget?: number) {
    if (confTarget) throw new RainbohError(RainbohError.RPC_INVALID_PARAMS)
    return this.coreClient.sendToAddress(address, amount)
  }

  public sendMany(amounts: IBtcSendManyOutputs, confTarget?: number, estimateMode?: EstimateMode): string {
    if (confTarget || estimateMode) throw new RainbohError(RainbohError.RPC_INVALID_PARAMS)
    return this.coreClient.sendMany('', amounts)
  }

  public async issueAsset(address: string, amount: number, divisibility: number) {
    if (!BitcoinHelper.isValidAddress(address)) throw new RainbohError(RainbohError.INVALID_ADDRESS)

    const capiUtxoBtc = await this.getCapiUtxos(1)

    // estimateSmartFee responses with errors if bitcoind doesn't have enough information to calculate fees
    const feeEstimation = await this.estimateSmartFee(3)
    // if (feeEstimation.errors) throw new RainbohError(RainbohError.FEE_ESTIMATION_NOT_AVAILABLE)
    if (!feeEstimation.feerate) logger.warn('Fee estimation not available. Fee set to 1000 sat/KB')
    const feeRate = feeEstimation.feerate || 0.00001 // fallback to 1 sat/B

    const feeSatPerKB = ColoredHelper.toSat(feeRate)

    logger.debug('Choose fee: %s sat/KB', feeSatPerKB)

    try {
      const { hex, assetId } = await ColoredHelper.createIssueTx(
        capiUtxoBtc,
        [{ address: address, amount: amount }],
        divisibility,
        feeSatPerKB,
        { walletBtc: this }
      )

      const hexSigned = await BitcoinHelper.signTx(hex, this)

      const txid = await this.sendRawTransaction(hexSigned)
      logger.debug('tx %s broadcasted!', txid)

      return { txid: txid, assetId: assetId }
    } catch (err) {
      if (err.message.includes('No output with the requested asset'))
        throw new RainbohError(RainbohError.NOT_ENOUGH_CONFIRMED_ASSETS)

      if (err.message.includes('Not enough satoshi to cover transaction'))
        throw new RainbohError(RainbohError.NOT_ENOUGH_CONFIRMED_BITCOIN)

      throw err
    }
  }

  public async getCapiUtxos(minConf: number = 0) {
    let listUnspent

    try {
      listUnspent = await this.listUnspent(minConf)
    } catch (err) {
      // Requested wallet does not exist or is not loaded
      if (err.code === -18) throw new RainbohError(RainbohError.WALLET_BTC_NOT_LOADED)
      else throw err
    }

    const capiUtxos: CapiUtxo[] = []

    for (const unspent of listUnspent) {
      capiUtxos.push({
        txid: unspent.txid,
        index: unspent.vout,
        value: ColoredHelper.toSat(unspent.amount),
        used: false,
        scriptPubKey: {
          addresses: [unspent.address],
          hex: unspent.scriptPubKey
        },
        assets: []
      })
    }

    return capiUtxos
  }

  public async createPsbt(inputs: ICreatePsbtInputs, outputs: IBtcCreatePsbtOutputs): Promise<string> {
    return this.coreClient.createPsbt(inputs, outputs)
  }
}

export default WalletBtc

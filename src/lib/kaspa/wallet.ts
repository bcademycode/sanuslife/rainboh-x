/*
 *    Copyright 2024 Bcademy s.r.l.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import path from 'path'
import fs from 'fs'
import ConfigProvider from '../../utils/configProvider'
import {
  Address,
  createTransaction,
  Mnemonic,
  NetworkType,
  Resolver,
  RpcClient,
  setDefaultStorageFolder,
  signTransaction,
  Wallet,
  XPrv
} from '../../../kaspa'
import { ICreateAccountResponse } from './models/ICreateAccountResponse'
import { IAccount } from './models/IAccount'
import { ISpendResponse } from './models/ISpendResponse'

// @ts-ignore
import { w3cwebsocket } from 'websocket'
import RainbohError from '../../error/rainbohError'

globalThis.WebSocket = w3cwebsocket

class WalletKaspa {
  public static async create() {
    const instance = new WalletKaspa()
    await instance.configureStorage()
    return instance
  }

  protected readonly configProvider: ConfigProvider
  private readonly walletPath: string
  private readonly filename: string
  private readonly secret: string
  private readonly mnemonic: string
  private readonly network: 'testnet-10' | 'testnet-11' | 'mainnet'

  private constructor() {
    this.configProvider = ConfigProvider.getInstance()
    const config = this.configProvider.getConfig().kaspa
    this.walletPath = config.wallet.path
    this.filename = config.wallet.filename
    this.secret = config.wallet.secret
    this.network = config.network
    this.mnemonic = config.wallet.mnemonic
  }

  public async getNewAddress(accountName: string) {
    const wallet = await this.getWallet()

    // create new bip32 key node
    const prvKeyData = await wallet.prvKeyDataCreate({
      walletSecret: this.secret,
      mnemonic: this.mnemonic
    })

    // new account linked to previous bip32 node
    const account = await wallet.accountsCreate({
      walletSecret: this.secret,
      type: 'bip32',
      accountName: accountName,
      prvKeyDataId: prvKeyData.prvKeyDataId
    })

    if (account && account.accountDescriptor && account.accountDescriptor.receiveAddress) {
      const returnData = {
        id: account.accountDescriptor.accountId,
        accountIndex: account.accountDescriptor.accountIndex,
        name: account.accountDescriptor.accountName,
        address: `${account.accountDescriptor.receiveAddress.prefix}:${account.accountDescriptor.receiveAddress.payload}`
      } as IAccount
      return {
        account: returnData
      } as ICreateAccountResponse
    }
  }

  public async spend(accountIndex: number, recipient: string): Promise<ISpendResponse> {
    if (!Address.validate(recipient)) throw new RainbohError(RainbohError.INVALID_ADDRESS)

    const resolver = new Resolver()
    const rpc = new RpcClient({
      resolver,
      networkId: this.network
    })
    await rpc.connect()

    const networkType = this.network === 'mainnet' ? NetworkType.Mainnet : NetworkType.Testnet

    // get private key from account index
    const seed = new Mnemonic(this.mnemonic).toSeed()
    const xPrv = new XPrv(seed)
    const privateKey = xPrv.derivePath(`m/44'/111111'/${accountIndex}'/0/0`)
    const pubInfo = privateKey
      .toXPub()
      .toPublicKey()
      .toAddress(networkType)

    const sender = `${pubInfo.prefix}:${pubInfo.payload}`
    const signingKey = privateKey.toPrivateKey().toString()

    // get utxo from sender address
    const { entries: utxos } = await rpc.getUtxosByAddresses([sender])

    // calculate the total spending
    const total = utxos.reduce((agg: any, curr: any) => {
      return curr.amount + agg
    }, 0n)

    // set output address and amount (subtract fees)
    const outputs = [
      {
        address: recipient,
        amount: total - BigInt(utxos.length) * 2000n
      }
    ]

    // create, sign, broadcast tx
    const tx = createTransaction(utxos, outputs, 0n)
    const transaction = signTransaction(tx, [signingKey], true)
    const { transactionId } = await rpc.submitTransaction({ transaction })

    // disconnect RPC and exit
    await rpc.disconnect()

    return {
      txids: [transactionId]
    } as ISpendResponse
  }

  public async listAccounts(count: number, skip: number) {
    const wallet = await this.getWallet()
    const walletAccounts = await wallet.accountsEnumerate({})

    const accounts: IAccount[] = walletAccounts.accountDescriptors.map((account: any) => ({
      id: account.accountId,
      accountIndex: account.accountIndex,
      name: account.accountName,
      address: `${account.receiveAddress.prefix}:${account.receiveAddress.payload}`
    }))

    return accounts.slice(skip, skip + count)
  }

  public isValidAddress(address: string) {
    return Address.validate(address)
  }

  private async configureStorage() {
    const storageFolder = path.join(this.walletPath).normalize()
    if (!fs.existsSync(storageFolder)) {
      fs.mkdirSync(storageFolder)
    }
    setDefaultStorageFolder(storageFolder)

    // create wallet if it does not exist
    const wallet = new Wallet({
      resident: false,
      networkId: this.network,
      resolver: new Resolver()
    })

    if (!(await wallet.exists(this.filename))) {
      await wallet.walletCreate({
        walletSecret: this.secret,
        filename: this.filename,
        title: this.filename
      })
    }
  }

  private async getWallet() {
    const wallet = new Wallet({
      resident: false,
      networkId: this.network,
      resolver: new Resolver()
    })

    // stop and throw error if wallet does not exist
    if (!(await wallet.exists(this.filename))) throw new RainbohError(RainbohError.INVALID_WALLET).toObject()

    // open wallet
    await wallet.walletOpen({
      walletSecret: this.secret,
      filename: this.filename,
      accountDescriptors: false
    })
    return wallet
  }
}

export default WalletKaspa

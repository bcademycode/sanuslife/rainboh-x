export interface IAccount {
  id: string
  accountIndex: number
  name: string
  address: string
}

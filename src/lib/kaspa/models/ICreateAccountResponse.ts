import { IAccount } from './IAccount'

export interface ICreateAccountResponse {
  account: IAccount
}

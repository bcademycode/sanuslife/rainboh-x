/*
 *    Copyright 2020 inbitcoin s.r.l.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import { IErrorCode } from '../interfaces/rainbohError'

class RainbohError extends Error {
  public static ASSERT: IErrorCode = { code: 1, message: 'Assert failed! Extreme situation. Check the stack trace.' }

  public static NOT_IMPLEMENTED: IErrorCode = { code: 100, message: 'Not implemented' }
  public static BITCOIND_VERSION_NOT_SUPPORTED: IErrorCode = { code: 101, message: 'Bitcoind version not supported.' }
  public static BITCOIND_NETWORK_MISMATCH: IErrorCode = {
    code: 102,
    message: 'Bitcoind network is different from config network'
  }

  public static MISSING_BTC_WALLET: IErrorCode = { code: 201, message: 'Missing btcWallet for selected wallet' }
  public static FEE_ESTIMATION_NOT_AVAILABLE: IErrorCode = { code: 202, message: 'Fee estimation not available' }
  public static NOT_ENOUGH_CONFIRMED_ASSETS: IErrorCode = {
    code: 203,
    message: 'Not enough confirmed assets in wallet'
  }
  public static NOT_ENOUGH_CONFIRMED_BITCOIN: IErrorCode = {
    code: 204,
    message: 'Not enough confirmed bitcoin in bitcoin wallet'
  }
  public static WALLET_BTC_NOT_LOADED: IErrorCode = { code: 205, message: 'Wallet bitcoin is not loaded' }
  public static INVALID_ADDRESS: IErrorCode = { code: 206, message: 'Invalid address' }
  public static INVALID_WALLET: IErrorCode = { code: 217, message: 'The requested wallet is not in the configuration' }
  public static INVALID_COIN: IErrorCode = { code: 218, message: 'The requested coin is not in the configuration' }
  public static WALLET_NOT_SPECIFIED: IErrorCode = {
    code: 219,
    message: 'Wallet file not specified (must request wallet RPC through /wallet/<filename> uri-path)'
  }
  public static CONFIG_NOT_LOADED: IErrorCode = { code: 220, message: 'Error loading config' }
  public static WALLET_BTC_ONLY: IErrorCode = { code: 221, message: 'Operation available only on bitcoin wallet' }
  public static WALLET_COLORED_ONLY: IErrorCode = { code: 222, message: 'Operation available only on colored wallet' }
  public static TX_INPUTS_NOT_FOUND: IErrorCode = { code: 223, message: 'Transaction inputs not found' }
  public static INVALID_ADDRESS_TYPE: IErrorCode = { code: 224, message: 'Invalid address type' }

  public static INVALID_JSON: IErrorCode = { code: -1, message: 'JSON value is not an object as expected' }
  public static INVALID_AMOUNT: IErrorCode = { code: -3, message: 'Invalid amount' }
  public static INVALID_DUMMY: IErrorCode = { code: -8, message: 'Dummy value must be set to ""' }
  public static DUPLICATE_ADDRESS: IErrorCode = { code: -8, message: 'Invalid parameter, duplicated address' }

  public static RPC_INVALID_PARAMS: IErrorCode = { code: -32602, message: 'Invalid method parameter(s)' }
  public static RPC_INTERNAL_ERROR: IErrorCode = { code: -32603, message: 'Internal JSON-RPC error' }

  // -32000 to -32099	Server error	Reserved for implementation-defined server-errors.
  public static RPC_GENERIC_SERVER_ERROR: IErrorCode = { code: -32000, message: 'Server error' }

  public static DB_COLLECTION_NOT_FOUND = (collectionName: string): IErrorCode => ({
    code: 301,
    message: `Collection "${collectionName}" not found`
  })

  public readonly code: number
  public readonly description?: string

  constructor(errorCode: IErrorCode, description?: any) {
    super(errorCode.message)

    this.code = errorCode.code
    this.message = errorCode.message

    if (description) this.description = description

    Error.captureStackTrace(this, this.constructor)
  }

  public toObject() {
    const err: { code: number; message: string; name: string; description?: string } = {
      code: this.code,
      message: this.message,
      name: RainbohError.name
    }
    if (this.description) err.description = this.description
    return err
  }
}

export default RainbohError

/*
 *    Copyright 2020 inbitcoin s.r.l.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

/**
 * This interface defines some Wallet interfaces
 */

interface ICreatePsbtInput {
  txid: string
  vout: number
  sequence?: number
}

export interface IAmountColBtc {
  amount: number
  amountBtc?: number
}

interface IColCreatePsbtOutput {
  [address: string]: number | IAmountColBtc
}

interface IBtcCreatePsbtOutput {
  [address: string]: number
}

export interface IColSendManyOutputs {
  [address: string]: number | IAmountColBtc
}

export interface IBtcSendManyOutputs {
  [address: string]: number
}

export type ICreatePsbtInputs = ICreatePsbtInput[]
export type IColCreatePsbtOutputs = IColCreatePsbtOutput[]
export type IBtcCreatePsbtOutputs = IBtcCreatePsbtOutput[]

export type EstimateMode = 'UNSET' | 'ECONOMICAL' | 'CONSERVATIVE'

/*
 *    Copyright 2020 inbitcoin s.r.l.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

/**
 * This interface define the config structure
 *
 * For details check docs/CONFIGURATION.md
 */

export interface IWalletBtcConfig {
  name: string
}

export interface IWalletColConfig {
  name: string
  coin: string
  btcWallet: string
}

export interface IColoredConfig {
  coin: string
  name: string
  addressPrefix: string
  addressHrp: string
  assetId: string
  divisibility: number
}

export default interface IRainbohConfig {
  version: string
  network: string
  wallets: {
    bitcoin: IWalletBtcConfig[]
    colored: IWalletColConfig[]
    coloredOptions: {
      softMaxUtxos: number
    }
  }
  coloredCoins: IColoredConfig[]
  capi: {
    db: {
      host: string
      port: string
      name: string
    }
  }
  bitcoind: {
    version: string
    host: string
    rpcPort: string
    rpcUsername: string
    rpcPassword: string
    timeout?: number
  }
  kaspa: {
    wallet: {
      mnemonic: string
      filename: string
      path: string
      secret: string
      title: string
    }
    network: 'testnet-10' | 'testnet-11' | 'mainnet'
  }
}

/*
 *    Copyright 2020 inbitcoin s.r.l.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import errorHandler from 'errorhandler'

import logger from './utils/logger'
import app from './app'
import CapiDbProvider from './utils/capiDbProvider'
import ConfigProvider from './utils/configProvider'
import RpcHelper from './utils/rpcHelper'

/**
 * Error Handler. Provides full stack - remove for production
 */
if (app.get('env') === 'development') {
  // only use in development
  app.use(errorHandler())
}

/**
 * Start Express server.
 */
const server = app.listen(app.get('port'), async () => {
  // Check if config exists and is valid
  let configProvider
  try {
    configProvider = ConfigProvider.getInstance()
  } catch (err) {
    logger.error('Config validation error: %s', err.message)
    process.exit(1)
  }

  logger.info('Config validated with success')
  logger.info(
    'Available bitcoin wallets: %s',
    configProvider
      .getConfig()
      .wallets.bitcoin.map(w => `${w.name}`)
      .join(', ')
  )
  logger.info(
    'Available colored wallets: %s',
    configProvider
      .getConfig()
      .wallets.colored.map(w => `${w.name}`)
      .join(', ')
  )

  try {
    await CapiDbProvider.getInstance().connect()
  } catch (err) {
    logger.error('Connection to Capi DB error: %s', err.message)
    process.exit(1)
  }

  try {
    await RpcHelper.checkConnection()
  } catch (err) {
    logger.error('Connection bitcoind error: %s', err.message)
    process.exit(1)
  }

  logger.info('  RPC service is listening on port %d in %s mode', app.get('port'), app.get('env'))
  logger.info('  Press CTRL-C to stop\n')
})

server.setTimeout(Number(process.env.RPC_TIMEOUT) * 1000 || 120000)

export default server

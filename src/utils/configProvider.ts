/*
 *    Copyright 2020 inbitcoin s.r.l.
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

import _ from 'lodash'
import * as fs from 'fs'
import * as path from 'path'
import { networks } from 'bitcoinjs-lib'

import RainbohError from '../error/rainbohError'
import IRainbohConfig, { IWalletBtcConfig, IWalletColConfig, IColoredConfig } from '../interfaces/rainbohConfig'
import logger from './logger'

class ConfigProvider {
  public static CONF_PATH = path.join(__dirname, '../../rainboh-config.json')
  public static SUPPORTED_CONFIG_VERSIONS = ['2.0']
  public static SUPPORTED_BITCOIND_VERSIONS = ['18', '21', '22', '23', '25']
  public static WALLET_TYPE_BITCOIN = 'bitcoin'
  public static WALLET_TYPE_COLORED = 'colored'
  public static WALLET_TYPE_KASPA = 'kaspa'
  public static WALLET_TYPE_UNKNOWN = 'unknown'
  public static SAFE_TO_EXPORT = ['version', 'network', 'wallets', 'coloredCoins']

  public static getInstance() {
    if (!ConfigProvider._instance) {
      ConfigProvider._instance = new ConfigProvider()
    }
    return ConfigProvider._instance
  }

  private static _instance: ConfigProvider | null = null

  private static readConfigFile() {
    try {
      return JSON.parse(fs.readFileSync(ConfigProvider.CONF_PATH, 'utf8'))
    } catch (err) {
      logger.error(err.message)
      throw new RainbohError(RainbohError.CONFIG_NOT_LOADED)
    }
  }

  private rainbohConfig: IRainbohConfig

  private constructor() {
    const config = ConfigProvider.readConfigFile()
    if (!config) throw new RainbohError(RainbohError.CONFIG_NOT_LOADED)
    if (!this.isValidConfig(config)) throw new Error('Error during config validation')

    this.rainbohConfig = config

    if (process.env.CAPI_DB_HOST) this.rainbohConfig.capi.db.host = process.env.CAPI_DB_HOST
    if (process.env.CAPI_DB_PORT) this.rainbohConfig.capi.db.port = process.env.CAPI_DB_PORT
    if (process.env.CAPI_DB_NAME) this.rainbohConfig.capi.db.name = process.env.CAPI_DB_NAME

    if (process.env.BITCOIND_VERSION) this.rainbohConfig.bitcoind.version = process.env.BITCOIND_VERSION
    if (process.env.BITCOIND_HOST) this.rainbohConfig.bitcoind.host = process.env.BITCOIND_HOST
    if (process.env.BITCOIND_RPCPORT) this.rainbohConfig.bitcoind.rpcPort = process.env.BITCOIND_RPCPORT
    if (process.env.BITCOIND_RPCUSERNAME) this.rainbohConfig.bitcoind.rpcUsername = process.env.BITCOIND_RPCUSERNAME
    if (process.env.BITCOIND_RPCPASSWORD) this.rainbohConfig.bitcoind.rpcPassword = process.env.BITCOIND_RPCPASSWORD
    if (process.env.BITCOIND_TIMEOUT) this.rainbohConfig.bitcoind.timeout = parseInt(process.env.BITCOIND_TIMEOUT, 10)
  }

  public getConfig() {
    return this.rainbohConfig as IRainbohConfig
  }

  public getSafeConfig() {
    const config = this.getConfig() as { [key: string]: any }
    if (process.env.NODE_ENV === 'test') return config

    const safeConfig: { [key: string]: any } = {}
    for (const sectionName of ConfigProvider.SAFE_TO_EXPORT) {
      safeConfig[sectionName] = config[sectionName]
    }
    return safeConfig
  }

  public getWalletType(walletName: string) {
    if (Object.values(this.rainbohConfig.wallets.bitcoin).some(w => w.name === walletName))
      return ConfigProvider.WALLET_TYPE_BITCOIN
    else if (Object.values(this.rainbohConfig.wallets.colored).some(w => w.name === walletName))
      return ConfigProvider.WALLET_TYPE_COLORED
    else return ConfigProvider.WALLET_TYPE_UNKNOWN
  }

  public getWalletBtcConfig(walletName: string): IWalletBtcConfig {
    const configWallet = _.find(this.rainbohConfig.wallets.bitcoin, { name: walletName })

    if (!configWallet) throw new RainbohError(RainbohError.INVALID_WALLET)
    return configWallet
  }

  public getWalletColConfig(walletName: string): IWalletColConfig {
    const configWallet = _.find(this.rainbohConfig.wallets.colored, { name: walletName })

    if (!configWallet) throw new RainbohError(RainbohError.INVALID_WALLET)
    return configWallet
  }

  public getColoredConfig(coin: string): IColoredConfig {
    const coloredConfig = _.find(this.rainbohConfig.coloredCoins, { coin: coin })

    if (!coloredConfig) throw new RainbohError(RainbohError.INVALID_COIN)
    return coloredConfig
  }

  /**
   * Check if a wallet with <wallet_name> is present in config
   * @param walletName
   */
  public isValidWalletName(walletName: string): boolean {
    const walletNames = [
      ...this.rainbohConfig.wallets.bitcoin.map(w => w.name),
      ...this.rainbohConfig.wallets.colored.map(w => w.name)
    ]
    return walletNames.includes(walletName)
  }

  /**
   * Check if the configuration respect the rules.
   * Returns false when it detects an invalid configuration, logging the first
   * encountered problem.
   */
  public isValidConfig(cfg: any) {
    const coloredCoins = cfg.coloredCoins
    const wallets = cfg.wallets
    const kaspa = cfg.kaspa

    // make sure necessary parameters are defined
    const necParams: { [key: string]: any } = {
      version: cfg.version,
      network: cfg.network,
      coloredCoins: cfg.coloredCoins,
      capi: cfg.capi,
      bitcoind: cfg.bitcoind,
      wallets: cfg.wallets,
      'wallets.bitcoin': _.get(cfg, 'wallets.bitcoin'),
      'wallets.colored': _.get(cfg, 'wallets.colored'),
      'wallets.coloredOptions': _.get(cfg, 'wallets.coloredOptions'),
      'wallets.coloredOptions.softMaxUtxos': _.get(cfg, 'wallets.coloredOptions.softMaxUtxos'),
      'capi.db': _.get(cfg, 'capi.db'),
      'capi.db.host': _.get(cfg, 'capi.db.host'),
      'capi.db.port': _.get(cfg, 'capi.db.port'),
      'capi.db.name': _.get(cfg, 'capi.db.name'),
      'bitcoind.version': _.get(cfg, 'bitcoind.version'),
      'bitcoind.host': _.get(cfg, 'bitcoind.host'),
      'bitcoind.rpcPort': _.get(cfg, 'bitcoind.rpcPort'),
      'bitcoind.rpcUsername': _.get(cfg, 'bitcoind.rpcUsername'),
      'bitcoind.rpcPassword': _.get(cfg, 'bitcoind.rpcPassword'),
      'kaspa.wallet.mnemonic': _.get(cfg, 'kaspa.wallet.mnemonic'),
      'kaspa.wallet.filename': _.get(cfg, 'kaspa.wallet.filename'),
      'kaspa.wallet.path': _.get(cfg, 'kaspa.wallet.path'),
      'kaspa.wallet.secret': _.get(cfg, 'kaspa.wallet.secret'),
      'kaspa.wallet.title': _.get(cfg, 'kaspa.wallet.title'),
      'kaspa.network': _.get(cfg, 'kaspa.network')
    }
    for (const paramKey in necParams) {
      if (!necParams.hasOwnProperty(paramKey)) continue
      if (!this.isParamDefined(paramKey, necParams[paramKey])) {
        return false
      }
    }

    // make sure config version is a string
    if (typeof cfg.version !== 'string') {
      logger.error('Config version must be a string.')
      return false
    }

    // make sure config version is supported
    if (!ConfigProvider.SUPPORTED_CONFIG_VERSIONS.includes(cfg.version)) {
      logger.error(
        'Unsupported config version. Allowed versions: %s',
        ConfigProvider.SUPPORTED_CONFIG_VERSIONS.join(', ')
      )
      return false
    }

    // make sure network has valid value
    const allowedNetworks = ['mainnet', 'testnet', 'regtest']
    if (!allowedNetworks.includes(cfg.network)) {
      logger.error('Invalid network value. Allowed values: %s', allowedNetworks.join(', '))
      return false
    }

    // make sure not use mainnet in test env
    if (process.env.NODE_ENV === 'test' && cfg.network === 'mainnet') {
      logger.error('Network mainnet cannot be used with NODE_ENV "test"')
      return false
    }

    // make sure wallets are valid
    if (!this.areValidWallets(wallets)) {
      return false
    }

    // make sure coloredCoins are valid
    if (!this.areValidColoredCoins(coloredCoins)) {
      return false
    }

    // make sure each 'coin' defined in colored wallets is used as 'coin' in coloredCoins
    const wCoins = [...new Set(_.map(wallets.colored, 'coin'))]
    const ccCoins = _.map(coloredCoins, 'coin')
    if (!wCoins.every(coin => ccCoins.includes(coin))) {
      // fail if wCoins is not a subset of ccCoins
      logger.error("Each colored wallet 'coin' must be defined under coloredCoins")
      return false
    }

    // make sure softMaxUtxos is an integer in the allowed range
    const softMaxUtxosLimit = { min: 1, max: 1000 }
    const softMaxUtxos = _.get(wallets, 'coloredOptions.softMaxUtxos')
    if (!this.isIntegerInLimits('softMaxUtxos', softMaxUtxos, softMaxUtxosLimit)) {
      return false
    }

    // if exists make sure bitcoind timeout is a valid integer
    const bitcoindTimeoutLimit = { min: 1000 }
    const bitcoindTimeout = _.get(cfg, 'bitcoind.timeout')
    if (bitcoindTimeout && !this.isIntegerInLimits('bitcoind.timeout', bitcoindTimeout, bitcoindTimeoutLimit)) {
      return false
    }

    if (!['mainnet', 'testnet-10', 'testnet-11'].includes(kaspa.network)) {
      return false
    }

    return true
  }

  public getNetwork(): 'mainnet' | 'testnet' | 'regtest' {
    const network = this.rainbohConfig.network
    if (['mainnet', 'testnet', 'regtest'].includes(network)) {
      return network as 'mainnet' | 'testnet' | 'regtest'
    }
    return 'mainnet'
  }

  public getNetworkBitcoinLib() {
    switch (this.rainbohConfig.network) {
      case 'mainnet':
        return networks.bitcoin
      case 'testnet':
        return networks.testnet
      case 'regtest':
        return networks.regtest
      default:
        return networks.bitcoin
    }
  }

  public getNetworkCapi() {
    switch (this.rainbohConfig.network) {
      case 'mainnet':
      case 'testnet':
        return this.rainbohConfig.network
      case 'regtest':
        return 'testnet'
      default:
        return 'mainnet'
    }
  }

  public loadConfigData(config: IRainbohConfig) {
    const configMerged = _.defaultsDeep(config, this.rainbohConfig)
    if (!this.isValidConfig(configMerged)) throw new Error('Error during config validation')
    this.rainbohConfig = config
    return this.rainbohConfig
  }

  private isIntegerInLimits(paramName: string, cfgValue: number, limits: { min?: number; max?: number }) {
    if (!Number.isInteger(cfgValue)) {
      logger.error("The parameter '%s' must be an integer", paramName)
      return false
    }
    if (limits.min !== undefined && cfgValue < limits.min) {
      logger.error("Parameter '%s' must be at least %d", paramName, limits.min)
      return false
    }
    if (limits.max !== undefined && cfgValue > limits.max) {
      logger.error("Parameter '%s' must be at most %d", paramName, limits.max)
      return false
    }
    return true
  }

  private isParamDefined(paramName: string, cfgValue: any) {
    if (!cfgValue) {
      logger.error("The '%s' parameter must be defined", paramName)
      return false
    }
    return true
  }

  private isParamDefinedInArray(arrayName: string, array: any, paramName: string) {
    if (_.filter(array, obj => obj[paramName] === undefined).length > 0) {
      logger.error("All %s must define the parameter '%s'", arrayName, paramName)
      return false
    }
    return true
  }

  private areValidWallets(wallets: any) {
    // make sure wallets bitcoin is an array
    if (!Array.isArray(wallets.bitcoin)) {
      logger.error("The parameter 'wallets.bitcoin' should be an array")
      return false
    }

    // make sure wallets colored is an array
    if (!Array.isArray(wallets.colored)) {
      logger.error("The parameter 'wallets.colored' should be an array")
      return false
    }

    // make sure each wallet has a name and a coin defined
    const necParamsBtc = ['name']
    for (const param of necParamsBtc) {
      if (!this.isParamDefinedInArray('wallets.bitcoin', wallets.bitcoin, param)) {
        return false
      }
    }

    const necParamsCol = ['name', 'coin', 'btcWallet']
    for (const param of necParamsCol) {
      if (!this.isParamDefinedInArray('wallets.colored', wallets.colored, param)) {
        return false
      }
    }

    // show warning if there are no colored wallets
    if (wallets.colored.length < 1) {
      logger.warning('There are no colored wallets defined')
    }

    // make sure each wallet 'name' is unique
    const walletNames = [...wallets.bitcoin, ...wallets.colored]
    if (!(walletNames.length === _.uniqBy(walletNames, 'name').length)) {
      logger.error("Wallet 'name' must be unique")
      return false
    }

    // make sure each colored wallet has a valid btcWallet
    const walletNamesBtc = wallets.bitcoin.map((w: any) => w.name)
    const walletsColored = wallets.colored
    if (!walletsColored.every((wColored: any) => wColored.btcWallet && walletNamesBtc.includes(wColored.btcWallet))) {
      logger.error('Each colored wallet must have a valid btcWallet')
      return false
    }

    return true
  }

  private areValidColoredCoins(coloredCoins: IColoredConfig[]) {
    // make sure coloredCoins is an array
    if (!Array.isArray(coloredCoins)) {
      logger.error("The parameter 'coloredCoins' should be an array")
      return false
    }

    // make sure each colored coin has a coin, a name, an addressPrefix, an assetId and a divisibility defined
    const necParams = ['coin', 'name', 'addressPrefix', 'addressHrp', 'assetId', 'divisibility']
    for (const param of necParams) {
      if (!this.isParamDefinedInArray('coloredCoins', coloredCoins, param)) {
        return false
      }
    }

    // show warning if there are no colored coins
    if (coloredCoins.length < 1) {
      logger.warning('There are no colored coins defined')
    }

    // make sure the coloredCoins 'coin' parameter is unique
    if (!(coloredCoins.length === _.uniqBy(coloredCoins, 'coin').length)) {
      logger.error("The coloredCoins 'coin' parameter must be unique")
      return false
    }

    // make sure divisibility is an integer in the allowed range
    const divisibility = { min: 0, max: 8 }
    for (const cc of coloredCoins) {
      if (!this.isIntegerInLimits('divisibility', cc.divisibility, divisibility)) {
        return false
      }
    }

    // addresses prefixes properties
    for (const cc of coloredCoins) {
      // addressPrefix must be only one char
      if (cc.addressPrefix.length !== 1) {
        logger.error('addressPrefix must be only one char')
        return false
      }
      // addressPrefix and addressHrp cannot share the prefix
      if (cc.addressPrefix === cc.addressHrp[0]) {
        logger.error('addressPrefix and addressHrp cannot share the prefix')
        return false
      }
    }

    return true
  }
}

export default ConfigProvider

import * as path from 'path'
import * as fs from 'fs'

export function getFixture(name: string) {
  const CONF_PATH = path.join(__dirname, '../fixtures/configs/', name)
  return JSON.parse(fs.readFileSync(CONF_PATH, 'utf8'))
}

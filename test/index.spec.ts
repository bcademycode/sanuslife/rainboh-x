import * as configUtils from './utils/config'
import ConfigProvider from '../src/utils/configProvider'
import ColoredHelper from '../src/utils/coloredHelper'
import WalletCol from '../src/lib/walletCol'
import WalletBtc from '../src/lib/walletBtc'
import Wallet from '../src/lib/wallet'
import RpcHelper from '../src/utils/rpcHelper'
import BitcoinHelper from '../src/utils/bitcoinHelper'

describe('WalletCol', () => {
  afterEach(() => {
    // @ts-ignore
    delete ConfigProvider._instance
    jest.restoreAllMocks()
  })

  describe('sendmany', () => {
    test('should create, sign and broadcast', async () => {
      const readConfig = jest.spyOn(ConfigProvider as any, 'readConfigFile')
      readConfig.mockImplementation(() => configUtils.getFixture('configTestnet.json'))

      const capiUtxosCol = [
        {
          txid: '37d7ae5905035a01b20e92efad6214eafed8c8f47565cc0eb1c3414abe295cb4',
          index: 0,
          value: 546,
          used: false,
          scriptPubKey: {
            addresses: ['mqYSZnREzeuXBYe6pNvx6AxCPahcpBsdea'],
            hex: '76a9146df945fe5f273bf9b84d2fe5014e2b172414983588ac'
          },
          assets: [
            {
              assetId: 'La8KsW5xXgrHpfyEtsEa1XEahJDTuGpRAoaH9T',
              amount: 10000000,
              issueTxid: 'c3c1a4e0befcb61661d9e0746ec1171757e040046d8b21178827e4526bc886be',
              divisibility: 7,
              lockStatus: true,
              aggregationPolicy: 'aggregatable'
            }
          ]
        }
      ]

      const capiUtxosBtc = [
        {
          txid: 'e1941bf86be64653402fc4075cad073be22bc7a9227c756e7a8a29780378af18',
          index: 0,
          value: 10000000,
          used: false,
          scriptPubKey: {
            addresses: ['mgEzmc6HrmJPt1bH4JSj2teaRepUygAjsA'],
            hex: '76a91407f389549b65a507bea74ba27540e16043dd4f9688ac'
          },
          assets: []
        }
      ]

      // Mock getCapiUtxos for btc wallet
      const spyGetCapiUtxosBtc = jest
        .spyOn(WalletBtc.prototype, 'getCapiUtxos')
        .mockImplementation(() => new Promise(resolve => resolve(capiUtxosBtc)))

      // Mock getCapiUtxos for col wallet
      const spyGetCapiUtxosCol = jest
        .spyOn(WalletCol.prototype, 'getCapiUtxos')
        .mockImplementation(() => new Promise(resolve => resolve(capiUtxosCol)))

      // Mock estimate smart fee
      const spyEstimateSmartFee = jest
        .spyOn(Wallet.prototype, 'estimateSmartFee')
        .mockImplementation(() => new Promise(resolve => resolve({ feerate: 0.00001405, blocks: 3 })))

      // Mock change address btc
      const spyCreateColTx = jest
        .spyOn(ColoredHelper, 'createTx')
        .mockImplementation(
          () =>
            new Promise(resolve =>
              resolve(
                '0100000002b45c29be4a41c3b10ecc6575f4c8d8feea1462adef920eb2015a030559aed7370000000000ffffffff18af780378298a7a6e757c22a9c72be23b07ad5c07c42f405346e66bf81b94e10000000000ffffffff0422020000000000001976a914aff4e9089e9618c554b6d552896d6f630d4ab6e888ac22020000000000001976a9141ffa6b9c0d7247979547f816d7ee10f514fd06c688ac00000000000000000c6a0a43430215002056012056fb919800000000001976a914a9e75a213c5ef43cdeffeae16616c7b3f6b07e2688ac00000000'
              )
            )
        )

      // Mock change address col
      const spySignColTx = jest
        .spyOn(ColoredHelper, 'signTx')
        .mockImplementation(
          () =>
            new Promise(resolve =>
              resolve(
                '0100000002b45c29be4a41c3b10ecc6575f4c8d8feea1462adef920eb2015a030559aed737000000006a47304402202ed19c0e1602d6f90ac3bd92ace6e7c2940aca65b32f9360e8226252436c69cd0220612d1bbb6fddae91af9c69e2c2b6c28416fa36c28b5e9100318608a73a78f3e4012102c964dce9b65f76f840f8235faf444599f9ebc1f726305c482e2a7357f769e46effffffff18af780378298a7a6e757c22a9c72be23b07ad5c07c42f405346e66bf81b94e1000000006a47304402201ca019a65c77b9d01c0d8edbf961847b3e5fe40ca5ad38c563cc893638382b3a02202107b9ac95dc5eb6575bdc0692849daf61c6fce4baa58cf7018d069c0edb215801210248d42f29f40f226c9735a16fb06fcf695324a1d3e3d486c09c20a7e0baa5894fffffffff0422020000000000001976a914aff4e9089e9618c554b6d552896d6f630d4ab6e888ac22020000000000001976a9141ffa6b9c0d7247979547f816d7ee10f514fd06c688ac00000000000000000c6a0a43430215002056012056fb919800000000001976a914a9e75a213c5ef43cdeffeae16616c7b3f6b07e2688ac00000000'
              )
            )
        )

      // Mock sendRawTransaction
      const spySendRawTransaction = jest
        .spyOn(WalletCol.prototype, 'sendRawTransaction')
        .mockImplementation(
          () => new Promise(resolve => resolve('1c3f98a2dc1f75b8811ee171a6ead31085aa13f8dc634d7d83d0a9fa9339a1be'))
        )

      const walletCol = new WalletCol('wallet_col')

      const txid = await walletCol.sendMany({
        CmwZKwHjzzd7DPEfjXx6T9fW1r2fMiZyvsw: 0.5,
        Cmxj3XoGX4KH5sqTtpAMyXyubPN9BGm7Ybr: 0.5
      })

      expect(txid).toBe('1c3f98a2dc1f75b8811ee171a6ead31085aa13f8dc634d7d83d0a9fa9339a1be')
      expect(spyGetCapiUtxosBtc).toBeCalledTimes(1)
      expect(spyGetCapiUtxosCol).toBeCalledTimes(1)
      expect(spyEstimateSmartFee).toBeCalledTimes(1)
      expect(spyCreateColTx).toBeCalledTimes(1)
      expect(spySignColTx).toBeCalledTimes(1)
      expect(spySendRawTransaction).toBeCalledTimes(1)
    })
  })

  describe('sendtoaddress', () => {
    test('uses confTarget', async () => {
      const readConfig = jest.spyOn(ConfigProvider as any, 'readConfigFile')
      readConfig.mockImplementation(() => configUtils.getFixture('configTestnet.json'))

      const capiUtxosCol = [
        {
          txid: '37d7ae5905035a01b20e92efad6214eafed8c8f47565cc0eb1c3414abe295cb4',
          index: 0,
          value: 546,
          used: false,
          scriptPubKey: {
            addresses: ['mqYSZnREzeuXBYe6pNvx6AxCPahcpBsdea'],
            hex: '76a9146df945fe5f273bf9b84d2fe5014e2b172414983588ac'
          },
          assets: [
            {
              assetId: 'La8KsW5xXgrHpfyEtsEa1XEahJDTuGpRAoaH9T',
              amount: 10000000,
              issueTxid: 'c3c1a4e0befcb61661d9e0746ec1171757e040046d8b21178827e4526bc886be',
              divisibility: 7,
              lockStatus: true,
              aggregationPolicy: 'aggregatable'
            }
          ]
        }
      ]

      const capiUtxosBtc = [
        {
          txid: 'e1941bf86be64653402fc4075cad073be22bc7a9227c756e7a8a29780378af18',
          index: 0,
          value: 10000000,
          used: false,
          scriptPubKey: {
            addresses: ['mgEzmc6HrmJPt1bH4JSj2teaRepUygAjsA'],
            hex: '76a91407f389549b65a507bea74ba27540e16043dd4f9688ac'
          },
          assets: []
        }
      ]

      // Mock getCapiUtxos for btc wallet
      const spyGetCapiUtxosBtc = jest
        .spyOn(WalletBtc.prototype, 'getCapiUtxos')
        .mockImplementation(() => new Promise(resolve => resolve(capiUtxosBtc)))

      // Mock getCapiUtxos for col wallet
      const spyGetCapiUtxosCol = jest
        .spyOn(WalletCol.prototype, 'getCapiUtxos')
        .mockImplementation(() => new Promise(resolve => resolve(capiUtxosCol)))

      // Mock estimate smart fee
      const spyEstimateSmartFee = jest
        .spyOn(Wallet.prototype, 'estimateSmartFee')
        .mockImplementation(() => new Promise(resolve => resolve({ feerate: 0.00001405, blocks: 3 })))

      // Mock change address btc
      const spyCreateColTx = jest
        .spyOn(ColoredHelper, 'createTx')
        .mockImplementation(
          () =>
            new Promise(resolve =>
              resolve(
                '0100000002b45c29be4a41c3b10ecc6575f4c8d8feea1462adef920eb2015a030559aed7370000000000ffffffff18af780378298a7a6e757c22a9c72be23b07ad5c07c42f405346e66bf81b94e10000000000ffffffff0422020000000000001976a914aff4e9089e9618c554b6d552896d6f630d4ab6e888ac22020000000000001976a9141ffa6b9c0d7247979547f816d7ee10f514fd06c688ac00000000000000000c6a0a43430215002056012056fb919800000000001976a914a9e75a213c5ef43cdeffeae16616c7b3f6b07e2688ac00000000'
              )
            )
        )

      // Mock change address col
      const spySignColTx = jest
        .spyOn(ColoredHelper, 'signTx')
        .mockImplementation(
          () =>
            new Promise(resolve =>
              resolve(
                '0100000002b45c29be4a41c3b10ecc6575f4c8d8feea1462adef920eb2015a030559aed737000000006a47304402202ed19c0e1602d6f90ac3bd92ace6e7c2940aca65b32f9360e8226252436c69cd0220612d1bbb6fddae91af9c69e2c2b6c28416fa36c28b5e9100318608a73a78f3e4012102c964dce9b65f76f840f8235faf444599f9ebc1f726305c482e2a7357f769e46effffffff18af780378298a7a6e757c22a9c72be23b07ad5c07c42f405346e66bf81b94e1000000006a47304402201ca019a65c77b9d01c0d8edbf961847b3e5fe40ca5ad38c563cc893638382b3a02202107b9ac95dc5eb6575bdc0692849daf61c6fce4baa58cf7018d069c0edb215801210248d42f29f40f226c9735a16fb06fcf695324a1d3e3d486c09c20a7e0baa5894fffffffff0422020000000000001976a914aff4e9089e9618c554b6d552896d6f630d4ab6e888ac22020000000000001976a9141ffa6b9c0d7247979547f816d7ee10f514fd06c688ac00000000000000000c6a0a43430215002056012056fb919800000000001976a914a9e75a213c5ef43cdeffeae16616c7b3f6b07e2688ac00000000'
              )
            )
        )

      // Mock sendRawTransaction
      const spySendRawTransaction = jest
        .spyOn(WalletCol.prototype, 'sendRawTransaction')
        .mockImplementation(
          () => new Promise(resolve => resolve('1c3f98a2dc1f75b8811ee171a6ead31085aa13f8dc634d7d83d0a9fa9339a1be'))
        )

      const walletCol = new WalletCol('wallet_col')

      const txid = await walletCol.sendToAddress('CmwZKwHjzzd7DPEfjXx6T9fW1r2fMiZyvsw', 0.5, 55)

      expect(txid).toBe('1c3f98a2dc1f75b8811ee171a6ead31085aa13f8dc634d7d83d0a9fa9339a1be')
      expect(spyGetCapiUtxosBtc).toBeCalledTimes(1)
      expect(spyGetCapiUtxosCol).toBeCalledTimes(1)
      expect(spyEstimateSmartFee).toBeCalledTimes(1)
      expect(spyEstimateSmartFee).toBeCalledWith(55)
      expect(spyCreateColTx).toBeCalledTimes(1)
      expect(spySignColTx).toBeCalledTimes(1)
      expect(spySendRawTransaction).toBeCalledTimes(1)
    })
  })

  describe('opreturnlimit', () => {
    test('returns a number', async () => {
      const readConfig = jest.spyOn(ConfigProvider as any, 'readConfigFile')
      readConfig.mockImplementation(() => configUtils.getFixture('configTestnet.json'))

      const walletCol = new WalletCol('wallet_col')
      const amounts = [0.0001, 0.002, 0.03, 0.4, 5]

      const n = await walletCol.opReturnLimit(amounts)
      expect(n).toBeLessThanOrEqual(amounts.length)
    })
  })
})

describe('ColoredHelper', () => {
  afterEach(() => {
    // @ts-ignore
    delete ConfigProvider._instance
    jest.restoreAllMocks()
  })

  test('should create a send to many colored tx', async () => {
    const readConfig = jest.spyOn(ConfigProvider as any, 'readConfigFile')
    readConfig.mockImplementation(() => configUtils.getFixture('configTestnet.json'))

    const capiUtxosCol = [
      {
        txid: '061a3f9553933ddbb4c7ad9308f2f492adac2f69761acfe15a3993ba4b09c012',
        index: 0,
        value: 654,
        used: false,
        scriptPubKey: {
          addresses: ['2N1A5kWNCgJpMdeaXMYuyMADUNa66wZLJaT'],
          hex: 'a91456c80cd469f67ba471c4acbdbae78038f87132be87'
        },
        assets: [
          {
            assetId: 'La8KsW5xXgrHpfyEtsEa1XEahJDTuGpRAoaH9T',
            amount: 10000000,
            issueTxid: 'c3c1a4e0befcb61661d9e0746ec1171757e040046d8b21178827e4526bc886be',
            divisibility: 7,
            lockStatus: true,
            aggregationPolicy: 'aggregatable'
          }
        ]
      }
    ]
    const capiUtxosBtc = [
      {
        txid: 'cca4b0a8e9ca72366e9498572b3cd17873873844f2babe5269d3c0f3ed91f2c8',
        index: 0,
        value: 100000000,
        used: false,
        scriptPubKey: {
          addresses: ['2MxXzdhUvRRY3q8w4PRz9nLsXH6Xwv5RTiV'],
          hex: 'a9143a048d5972311e8e7f14623433a857525e61761587'
        },
        assets: []
      }
    ]

    // Mock change address btc
    jest
      .spyOn(WalletBtc.prototype, 'getRawChangeAddress')
      .mockImplementation(() => new Promise(resolve => resolve('mkQBRMhCebuwaAbBgcZwy9EXguazYVfcTB')))

    // Mock change address col
    jest
      .spyOn(WalletCol.prototype, 'getRawChangeAddress')
      .mockImplementation(() => new Promise(resolve => resolve('Cn2ZXscHHCRRtA4sjv5eLKgyjp7dkmCRqhW')))

    const walletBtc = new WalletBtc('wallet_btc')
    const walletCol = new WalletCol('wallet_col')

    const coloredTxHex = await ColoredHelper.createTx(
      [...capiUtxosCol, ...capiUtxosBtc],
      [
        { address: 'CmnVoE2webKfATX4nrwJn4kMRdhWrfUjQmi', amount: 500000 },
        { address: 'CmoEVvwL8gxfnhuxz4zMTnTUEj56KNBgK4A', amount: 500000 }
      ],
      1000,
      { walletBtc: walletBtc, walletCol: walletCol }
    )

    expect(coloredTxHex).toBe(
      '010000000212c0094bba93395ae1cf1a76692facad92f4f20893adc7b4db3d9353953f1a060000000000ffffffffc8f291edf3c0d36952bebaf24438877378d13c2b5798946e3672cae9a8b0a4cc0000000000ffffffff0522020000000000001976a9144c90e4d355613e6136941d493752ff07012823b688ac22020000000000001976a91454a457dfc4813d5ed02183d89de0d91346f6086e88ac00000000000000000c6a0a43430215002055012055cadbf505000000001976a9143590ac95772eab0ab9a1e11a6ac0c2a1b09a9bf088ac22020000000000001976a914e6d780da705cbfd1dad45399e5d18823a59ca52f88ac00000000'
    )
  })

  test('should raise an error for a colored sendmany without btc', async () => {
    const readConfig = jest.spyOn(ConfigProvider as any, 'readConfigFile')
    readConfig.mockImplementation(() => configUtils.getFixture('configTestnet.json'))

    const capiUtxosCol = [
      {
        txid: '061a3f9553933ddbb4c7ad9308f2f492adac2f69761acfe15a3993ba4b09c012',
        index: 0,
        value: 654,
        used: false,
        scriptPubKey: {
          addresses: ['2N1A5kWNCgJpMdeaXMYuyMADUNa66wZLJaT'],
          hex: 'a91456c80cd469f67ba471c4acbdbae78038f87132be87'
        },
        assets: [
          {
            assetId: 'La8KsW5xXgrHpfyEtsEa1XEahJDTuGpRAoaH9T',
            amount: 10000000,
            issueTxid: 'c3c1a4e0befcb61661d9e0746ec1171757e040046d8b21178827e4526bc886be',
            divisibility: 7,
            lockStatus: true,
            aggregationPolicy: 'aggregatable'
          }
        ]
      }
    ]
    const capiUtxosBtc: any = []

    // Mock change address btc
    jest
      .spyOn(WalletBtc.prototype, 'getRawChangeAddress')
      .mockImplementation(() => new Promise(resolve => resolve('mkQBRMhCebuwaAbBgcZwy9EXguazYVfcTB')))

    // Mock change address col
    jest
      .spyOn(WalletCol.prototype, 'getRawChangeAddress')
      .mockImplementation(() => new Promise(resolve => resolve('Cn2ZXscHHCRRtA4sjv5eLKgyjp7dkmCRqhW')))

    const walletBtc = new WalletBtc('wallet_btc')
    const walletCol = new WalletCol('wallet_col')

    try {
      await ColoredHelper.createTx(
        [...capiUtxosCol, ...capiUtxosBtc],
        [
          { address: 'CmnVoE2webKfATX4nrwJn4kMRdhWrfUjQmi', amount: 500000 },
          { address: 'CmoEVvwL8gxfnhuxz4zMTnTUEj56KNBgK4A', amount: 500000 }
        ],
        1000,
        { walletBtc: walletBtc, walletCol: walletCol }
      )
    } catch (err) {
      expect(err.message).toEqual('Not enough satoshi to cover transaction')
    }
  })

  describe('address', () => {
    describe('conversion', () => {
      describe('bech32', () => {
        describe('testnet', () => {
          test('btcToAsset', () => {
            const readConfig = jest.spyOn(ConfigProvider as any, 'readConfigFile')
            readConfig.mockImplementation(() => configUtils.getFixture('configTestnet.json'))
            const coloredConfig = ConfigProvider.getInstance().getColoredConfig('col')
            const bitcoinAddress = 'tb1quqcnp3avzapgsw70wswvwftq0305qxvuryttl7'
            const assetAddress = ColoredHelper.toColAddress(bitcoinAddress, coloredConfig)
            expect(assetAddress).toEqual('tcc1quqcnp3avzapgsw70wswvwftq0305qxvup9jrcv')
          })
          test('assetToAsset', () => {
            const readConfig = jest.spyOn(ConfigProvider as any, 'readConfigFile')
            readConfig.mockImplementation(() => configUtils.getFixture('configTestnet.json'))
            const coloredConfig = ConfigProvider.getInstance().getColoredConfig('col')
            const alreadyAssetAddress = 'tcc1quqcnp3avzapgsw70wswvwftq0305qxvup9jrcv'
            const assetAddress = ColoredHelper.toColAddress(alreadyAssetAddress, coloredConfig)
            expect(assetAddress).toEqual(alreadyAssetAddress)
          })
          test('assetToBtc', () => {
            const readConfig = jest.spyOn(ConfigProvider as any, 'readConfigFile')
            readConfig.mockImplementation(() => configUtils.getFixture('configTestnet.json'))
            const coloredConfig = ConfigProvider.getInstance().getColoredConfig('col')
            const assetAddress = 'tcc1quqcnp3avzapgsw70wswvwftq0305qxvup9jrcv'
            const bitcoinAsset = ColoredHelper.toBtcAddress(assetAddress, coloredConfig)
            expect(bitcoinAsset).toEqual('tb1quqcnp3avzapgsw70wswvwftq0305qxvuryttl7')
          })
          test('btcToBtc', () => {
            const readConfig = jest.spyOn(ConfigProvider as any, 'readConfigFile')
            readConfig.mockImplementation(() => configUtils.getFixture('configTestnet.json'))
            const coloredConfig = ConfigProvider.getInstance().getColoredConfig('col')
            const alreadyBitcoinAsset = 'tb1quqcnp3avzapgsw70wswvwftq0305qxvuryttl7'
            const bitcoinAsset = ColoredHelper.toBtcAddress(alreadyBitcoinAsset, coloredConfig)
            expect(bitcoinAsset).toEqual(alreadyBitcoinAsset)
          })
        })
        describe('mainnet', () => {
          test('btcToAsset', () => {
            const readConfig = jest.spyOn(ConfigProvider as any, 'readConfigFile')
            readConfig.mockImplementation(() => configUtils.getFixture('configMainnet.json'))
            // NODE_ENV=test is not allowed on mainnet
            process.env.NODE_ENV = 'production'
            const coloredConfig = ConfigProvider.getInstance().getColoredConfig('col')
            process.env.NODE_ENV = 'test'
            const bitcoinAddress = 'bc1qjl8uwezzlech723lpnyuza0h2cdkvxvh54v3dn'
            const assetAddress = ColoredHelper.toColAddress(bitcoinAddress, coloredConfig)
            expect(assetAddress).toEqual('cc1qjl8uwezzlech723lpnyuza0h2cdkvxvhx5urde')
          })
          test('assetToBtc', () => {
            const readConfig = jest.spyOn(ConfigProvider as any, 'readConfigFile')
            readConfig.mockImplementation(() => configUtils.getFixture('configMainnet.json'))
            // NODE_ENV=test is not allowed on mainnet
            process.env.NODE_ENV = 'production'
            const coloredConfig = ConfigProvider.getInstance().getColoredConfig('col')
            process.env.NODE_ENV = 'test'
            const assetAddress = 'cc1qjl8uwezzlech723lpnyuza0h2cdkvxvhx5urde'
            const bitcoinAsset = ColoredHelper.toBtcAddress(assetAddress, coloredConfig)
            expect(bitcoinAsset).toEqual('bc1qjl8uwezzlech723lpnyuza0h2cdkvxvh54v3dn')
          })
        })
        describe('regtest', () => {
          test('btcToAsset', () => {
            const readConfig = jest.spyOn(ConfigProvider as any, 'readConfigFile')
            const config = configUtils.getFixture('configTestnet.json')
            config.network = 'regtest'
            readConfig.mockImplementation(() => config)

            const coloredConfig = ConfigProvider.getInstance().getColoredConfig('col')
            const bitcoinAddress = 'bcrt1q9apxm7m0xdf4g455fr66a773vg78sa0kdhd7nd'
            const assetAddress = ColoredHelper.toColAddress(bitcoinAddress, coloredConfig)
            expect(assetAddress).toEqual('tcc1q9apxm7m0xdf4g455fr66a773vg78sa0kdldmrk')
          })
          test('assetToAsset', () => {
            const readConfig = jest.spyOn(ConfigProvider as any, 'readConfigFile')
            const config = configUtils.getFixture('configTestnet.json')
            config.network = 'regtest'
            readConfig.mockImplementation(() => config)

            const coloredConfig = ConfigProvider.getInstance().getColoredConfig('col')
            const alreadyAssetAddress = 'tcc1q9apxm7m0xdf4g455fr66a773vg78sa0kdldmrk'
            const assetAddress = ColoredHelper.toColAddress(alreadyAssetAddress, coloredConfig)
            expect(assetAddress).toEqual(alreadyAssetAddress)
          })
          test('assetToBtc', () => {
            const readConfig = jest.spyOn(ConfigProvider as any, 'readConfigFile')
            const config = configUtils.getFixture('configTestnet.json')
            config.network = 'regtest'
            readConfig.mockImplementation(() => config)

            const coloredConfig = ConfigProvider.getInstance().getColoredConfig('col')
            const assetAddress = 'tcc1q9apxm7m0xdf4g455fr66a773vg78sa0kdldmrk'
            const bitcoinAsset = ColoredHelper.toBtcAddress(assetAddress, coloredConfig)
            expect(bitcoinAsset).toEqual('bcrt1q9apxm7m0xdf4g455fr66a773vg78sa0kdhd7nd')
          })
          test('btcToBtc', () => {
            const readConfig = jest.spyOn(ConfigProvider as any, 'readConfigFile')
            const config = configUtils.getFixture('configTestnet.json')
            config.network = 'regtest'
            readConfig.mockImplementation(() => config)

            const coloredConfig = ConfigProvider.getInstance().getColoredConfig('col')
            const alreadyBitcoinAsset = 'bcrt1q9apxm7m0xdf4g455fr66a773vg78sa0kdhd7nd'
            const bitcoinAsset = ColoredHelper.toBtcAddress(alreadyBitcoinAsset, coloredConfig)
            expect(bitcoinAsset).toEqual(alreadyBitcoinAsset)
          })
        })
      })
      describe('legacy', () => {
        test('btcToAsset', () => {
          const readConfig = jest.spyOn(ConfigProvider as any, 'readConfigFile')
          readConfig.mockImplementation(() => configUtils.getFixture('configTestnet.json'))
          const coloredConfig = ConfigProvider.getInstance().getColoredConfig('col')
          const bitcoinAddress = 'mr6tNG1ZH9a4Ydw8cbw72bWpSztLZLNzA7'
          const assetAddress = ColoredHelper.toColAddress(bitcoinAddress, coloredConfig)
          expect(assetAddress).toEqual('Cmr6tNG1ZH9a4Ydw8cbw72bWpSztLZLNzA7')
        })
        test('assetToBtc', () => {
          const readConfig = jest.spyOn(ConfigProvider as any, 'readConfigFile')
          readConfig.mockImplementation(() => configUtils.getFixture('configTestnet.json'))
          const coloredConfig = ConfigProvider.getInstance().getColoredConfig('col')
          const assetAddress = 'Cmr6tNG1ZH9a4Ydw8cbw72bWpSztLZLNzA7'
          const bitcoinAsset = ColoredHelper.toBtcAddress(assetAddress, coloredConfig)
          expect(bitcoinAsset).toEqual('mr6tNG1ZH9a4Ydw8cbw72bWpSztLZLNzA7')
        })
      })
    })
    describe('validity', () => {
      describe('legacy', () => {
        test('asset', () => {
          const readConfig = jest.spyOn(ConfigProvider as any, 'readConfigFile')
          readConfig.mockImplementation(() => configUtils.getFixture('configTestnet.json'))
          const coloredConfig = ConfigProvider.getInstance().getColoredConfig('col')

          const asset = 'Cmr6tNG1ZH9a4Ydw8cbw72bWpSztLZLNzA7'
          expect(ColoredHelper.isValidAddress(asset, coloredConfig)).toBeTruthy()
          const assetP2SH = 'C2N3oefVeg6stiTb5Kh3ozCSkaqmx91FDbsm'
          expect(ColoredHelper.isValidAddress(assetP2SH, coloredConfig)).toBeTruthy()
          const differentAsset = '_mr6tNG1ZH9a4Ydw8cbw72bWpSztLZLNzA7'
          expect(ColoredHelper.isValidAddress(differentAsset, coloredConfig)).toBeFalsy()
          const bitcoin = 'mr6tNG1ZH9a4Ydw8cbw72bWpSztLZLNzA7'
          expect(ColoredHelper.isValidAddress(bitcoin, coloredConfig)).toBeFalsy()
          const broken = 'Cmr6tNG1ZH9a4Ydw8cbw72bWpSztLZLXXXX'
          expect(ColoredHelper.isValidAddress(broken, coloredConfig)).toBeFalsy()
        })
        test('bitcoin', () => {
          const readConfig = jest.spyOn(ConfigProvider as any, 'readConfigFile')
          readConfig.mockImplementation(() => configUtils.getFixture('configTestnet.json'))

          const asset = 'Cmr6tNG1ZH9a4Ydw8cbw72bWpSztLZLNzA7'
          expect(BitcoinHelper.isValidAddress(asset)).toBeFalsy()
          const bitcoin = 'mr6tNG1ZH9a4Ydw8cbw72bWpSztLZLNzA7'
          expect(BitcoinHelper.isValidAddress(bitcoin)).toBeTruthy()
          const bitcoinP2SH = '2N3oefVeg6stiTb5Kh3ozCSkaqmx91FDbsm'
          expect(BitcoinHelper.isValidAddress(bitcoinP2SH)).toBeTruthy()
          const broken = 'mr6tNG1ZH9a4Ydw8cbw72bWpSztLZLXXXX'
          expect(BitcoinHelper.isValidAddress(broken)).toBeFalsy()
        })
      })
      describe('bech32', () => {
        test('asset', () => {
          const readConfig = jest.spyOn(ConfigProvider as any, 'readConfigFile')
          readConfig.mockImplementation(() => configUtils.getFixture('configTestnet.json'))
          const coloredConfig = ConfigProvider.getInstance().getColoredConfig('col')

          const asset = 'tcc1quqcnp3avzapgsw70wswvwftq0305qxvup9jrcv'
          expect(ColoredHelper.isValidAddress(asset, coloredConfig)).toBeTruthy()
          const differentAsset = 'tuu1quqcnp3avzapgsw70wswvwftq0305qxvu7jrl7c'
          expect(ColoredHelper.isValidAddress(differentAsset, coloredConfig)).toBeFalsy()
          const bitcoin = 'tb1quqcnp3avzapgsw70wswvwftq0305qxvuryttl7'
          expect(ColoredHelper.isValidAddress(bitcoin, coloredConfig)).toBeFalsy()
          const broken = 'tcc1quqcnp3avzapgsw70wswvwftq0305qxvup9XXXX'
          expect(ColoredHelper.isValidAddress(broken, coloredConfig)).toBeFalsy()
        })
        test('bitcoin', () => {
          const readConfig = jest.spyOn(ConfigProvider as any, 'readConfigFile')
          readConfig.mockImplementation(() => configUtils.getFixture('configTestnet.json'))

          const asset = 'tcc1quqcnp3avzapgsw70wswvwftq0305qxvup9jrcv'
          expect(BitcoinHelper.isValidAddress(asset)).toBeFalsy()
          const bitcoin = 'tb1quqcnp3avzapgsw70wswvwftq0305qxvuryttl7'
          expect(BitcoinHelper.isValidAddress(bitcoin)).toBeTruthy()
          const broken = 'tb1quqcnp3avzapgsw70wswvwftq0305qxvuryXXXX'
          expect(BitcoinHelper.isValidAddress(broken)).toBeFalsy()
        })
        test('regtest', () => {
          const readConfig = jest.spyOn(ConfigProvider as any, 'readConfigFile')
          const config = configUtils.getFixture('configTestnet.json')
          config.network = 'regtest'
          readConfig.mockImplementation(() => config)

          const bitcoin = 'bcrt1q9apxm7m0xdf4g455fr66a773vg78sa0kdhd7nd'
          expect(BitcoinHelper.isValidAddress(bitcoin)).toBeTruthy()
        })
      })
    })
  })
})

describe('RpcHelper', () => {
  it('isJsonKeyDuplicate', () => {
    const notDuplicateKeysStr =
      '{"TmtoAMysUzQtqbVsr7sZWBH3auXbdixC2Fg": 0.01, \'TmvvHfC5jiWr1Qw31KfQn7sSdwcYiBmDTPZ\': 1}'
    expect(RpcHelper.isJsonKeyDuplicate(notDuplicateKeysStr)).toBeFalsy()

    const duplicateKeysStr = '{"TmtoAMysUzQtqbVsr7sZWBH3auXbdixC2Fg": 0.01, \'TmtoAMysUzQtqbVsr7sZWBH3auXbdixC2Fg\': 1}'
    expect(RpcHelper.isJsonKeyDuplicate(duplicateKeysStr)).toBeTruthy()
  })
})
